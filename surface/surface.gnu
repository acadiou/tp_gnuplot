set style line 100 lt -1 lw 0.1
set palette defined ( 0 '#000090',\
                      1 '#000fff',\
                      2 '#0090ff',\
                      3 '#0fffee',\
                      4 '#90ff70',\
                      5 '#ffee00',\
                      6 '#ff7000',\
                      7 '#ee0000',\
                      8 '#7f0000')
unset colorbox
set view 65,35
set nokey
set hidden3d
set isosamples 32
set term pdf size 3in,3in
set output 'surface.pdf'
set xrange [-4:+4]
set yrange [-4:+4]
set zrange [-1:+1]
set ticslevel 0
set pm3d
set pm3d solid hidden3d 100
x=0.0
set linetype 1 lc rgb "black"
splot sin(sqrt(x*x+y*y)) w l
