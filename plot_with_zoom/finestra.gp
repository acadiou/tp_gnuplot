set multiplot
xzoom = 4.
yzoom = 30.
xpos = 4.
ypos = 20.
dy = 2.
dx = 2.
set object 1 ellipse center xzoom, yzoom+dy/2 size dx/2, dy
set arrow from xzoom, yzoom to xpos, ypos+1.5*dy
set grid
set key right
set title 'n = 3.57e-4'
set xlabel 'T (MeV)'
set ylabel 'Helmotz Free Energy (MeV)'
eos = 'feos.txt'
second = 'fint.txt'
fourth = 'fd4.txt'
plot eos t 'EoS', second w lp t 'Interpolation with 2order derivatives', fourth u 1:2 w lp t 'Interpolation with 4order derivatives'
set origin 0.03*xpos, 0.005*ypos
set size 0.4,0.4
clear
unset key
unset title
unset object
unset arrow
unset ylabel
set xrange [xzoom-dx:xzoom+dx]
set yrange [yzoom-dy:yzoom+dy]
#set format y "%.1e"
set format x "%.2f"
set ytics 1
set xtics 1
plot eos w points ps 5 , second w lp ps 2, fourth w lp
