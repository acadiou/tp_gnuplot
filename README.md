# Documentation

* [http://www.gnuplot.info/](http://www.gnuplot.info/)

# Exemples de scripts gnuplot

- tracé d'une courbe XY normalisée par rapport au premier instant ([script](courbeXY/courbeXY.script)) ([image](courbeXY/courbeXY.png)) ([data](courbeXY/data.zip))
- tracé en log ([script](logScale/logScale.script)) ([image](logScale/logScale.png)) ([data](logScale/residu.res))
- tracé d'une surface 3D ([script](surface/surface.gnu)) ([image](surface/surface.pdf))
- dans LaTeX ([tex](LaTeX/latex.tex)) ([pdf](LaTeX/latex.pdf)) ([bash](LaTeX/compile.sh))
